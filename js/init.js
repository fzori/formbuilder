

$(document).ready(function(){


	getForm();

}); // end of document ready


function getForm()
{

	$.getJSON( _GET_FORM_URL , { client_id: 0  } )
  	.done(function( json )
  	{
  		createForm( json );
  		setFormTitle( json.title );
  		_ACTION = json.action;

  		// recorrermos campos para pintar
  		$.each(json.fields, function( i , field )
  		 {
  		 	var 	newField = createDivGroup();
    			// add element label
    		newField.append( setElementLabel( field.fieldCaption ) );
    		newField.append( setElement( field ) );
  		});


  		// all forms need a button
				createButton();
  				// es un elemnto dinamico, aplicamos eventos
  				FormButtonSetEvent();

  				setDatePicker();
  				setDateTimePicker();
  				setTimePicker();
  				setMultipleSelect();

  	})
  	.fail(function( jqxhr, textStatus, error ) {
    var err = textStatus + ", " + error;
    console.log( "Request Failed: " + err );
	});
}

function createForm( oJson )
{
	_FORM = $('<form/>');
	_FORM.attr('id','thisForm');
	_FORM.addClass('form-horizontal');
	_FIELDSET = $('<fieldset>');
	var legend = $( '<legend/>' );
	legend.attr('id','formLegend');
	legend.appendTo( _FIELDSET );

	_FIELDSET.appendTo( _FORM );

	var   divFormGroup  = $('<div/>');
	divFormGroup.addClass('form-group');
	divFormGroup.appendTo( _FIELDSET  );
	_FORM.appendTo( $('.form_container' ) );

}
function setFormTitle( sTitle )
{
	$( '#formLegend').html( sTitle );
}
function createDivGroup()
{
	var 	divgroup = $('<div/>');
	divgroup.addClass( 'col-sm-6 divgroup' );
	divgroup.appendTo( _FIELDSET );
	return divgroup;
}
function setElementLabel( sLabel )
{
	var 	label = $('<label/>');
	label.addClass( 'col-sm-4 control-label' );
	label.attr('for', sLabel);
	label.html( sLabel );
	return label;
}
function setElement( oField )
{

	var 	div = $( '<div/>');
	div.addClass( 'col-sm-8' );

	if( oField.type )
	{
		var 	input = $( aTags[ oField.type ] );
		input.attr( 'type' , aTypes[ oField.type  ] );
		input.attr('id', oField.fieldCaption );
		input.attr('name', oField.fieldCaption );
		input.attr('multiple', isMultiple[ oField.type ]  );
		input.addClass('form-control input-md ' + aClass[ oField.type ]);
		input.val( ( ( oField.fieldValue != 'null' ) ? oField.fieldValue : '' ) );

		input.appendTo( div );
	}

	if( oField.values )
	{
		$.each( oField.values , function( index , caption )
  		{
  				var 	option = $(  "<option/>"  );
  				console.log( caption.index + " " + caption.caption );
  				option.val( caption.index );
  				option.text( caption.caption );
   				option.appendTo( input );

  				if( caption.selected )
  				{
  					input.val( caption.index  );
  				} // END OF IF
  		});
	}

	return div;

}
function createInput()
{

}
function setElementVal( oElement , val )
{
	oElement.val( val );
}
function createButton()
{

	var 	button= $('<button/>');
	button.attr( 'id' , 'getReportButton' );
	button.attr( 'name' , 'getReportButton' );
	button.html( 'Send' );
	button.addClass( 'btn btn-primary' );
	button.appendTo( $('.button_container' ) );

}

function FormButtonSetEvent()
{
	$( "#getReportButton" ).click(function( event )
	{
		  sendRequest();
		  event.preventDefault();
		  return false;
	});
}

function setDatePicker()
{

	$('.date').datetimepicker({
  		format:'Y-m-d',
  		inline:false,
  		lang:'es',
 		timepicker:false
	});
}

function setDateTimePicker()
{

	$('.datetime').datetimepicker({
  		format:'Y-m-d H:i',
  		inline:false,
  		lang:'es',
 		timepicker:true
	});

}
function setTimePicker()
{
	$('.time').datetimepicker({
  	datepicker:false,
  	format:'H:i'
	});
}

function setMultipleSelect()
{
//	$(".multiselect").change(function() {
  //         console.log($(this).val());
    //    }).multipleSelect({
      //      width: '100%'
       // });
}

function sendRequest()
{

dynatable_setup();

var oData = [];
var oDataChart=[];
var aTitle = [];

console.log( $('#Estado').val().toString() );
	  $.getJSON( _ACTION, {
	    format: "json"
	  })
    .done(function( data ) {

      $.each( data.table, function( i, item )
      {

        	if( i == "rowTitles")
        	{
        		if( data.chart )
   		     {
        			 oDataChart.push( item );
        		} // end of if
        		$.each( item , function( iTitle, itemTitle )
        		{
    				aTitle[iTitle] = itemTitle.toLowerCase(); // almacenamos cabeceras
    			});
        	}

        	if( i=='rows')
        	{
        	 var chartElements = [];
				$.each( item , function( Ir, itemRow ) {

        				$.each( itemRow , function( index, col ) {
        				var aData =[];

        					//sabemos el numerod de columnas por los titulos
        						var auxelement = {};

        							for( var i=0;i< aTitle.length; i++ )
        							{
        									 auxelement[aTitle[i]] =  col[i];
        							} // end of for
        						if( data.chart )
								{
        							oDataChart.push( col );// añadimos elmennto al data del gráfico
        						}
        						oData.push( auxelement ); // añadimos elemento
       					});



       			});
			}
       });
       createTable( aTitle );

    // oData=[{"favcolor":"Red","persons":"6"},{"favcolor":"Blue","persons":"4"},{"favcolor":"Green","persons":"3"},{"favcolor":"Yellow","persons":"1"},{"favcolor":"Black","persons":"1"}]

        //var  oData2=[["favcolor","persons"],["Red",6],["Blue",4],["Green",3],["Yellow",1],["Black",1]]

          	 $('#reports').dynatable({
        		dataset: {
            		records:oData
       			 }
    		}); // end of dynatable

    			if( data.chart )
				{
    				drawChart( data.chart , oDataChart );
    			}
    });



}
function createTable( aTitle )
{
	var table = $('<table/>');
	table.attr('id','reports');
	table.addClass('table table-striped');

	var thead = $('<thead/>');
	thead.appendTo( table );

	var tr = $('<tr/>');
	tr.appendTo(thead );


	for( var i=0;i< aTitle.length; i++ )
        {
        	var th = $('<th>');
        	th.html(aTitle[i] );
        	th.appendTo( tr );
 		}
 	var tbody = $('<tbody/>');


	tbody.appendTo( table );
	$('#report_container').empty(); // eliminamos previos informes
	table.appendTo( $('#report_container') );
}
