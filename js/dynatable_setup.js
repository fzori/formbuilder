function dynatable_setup( )
{

$.dynatableSetup({
  		
		inputs: {
    queries: null,
    sorts: null,
    multisort: ['ctrlKey', 'shiftKey', 'metaKey'],
    page: null,
    queryEvent: 'blur change',
    recordCountTarget: null,
    recordCountPlacement: 'after',
    paginationLinkTarget: null,
    paginationLinkPlacement: 'after',
    paginationPrev: 'Anterior',
    paginationNext: 'Siguiente',
    paginationGap: [1,2,2,1],
    searchTarget: null,
    searchPlacement: 'before',
    perPageTarget: null,
    perPagePlacement: 'before',
    perPageText: 'Mostrando: ',
    recordCountText: 'Mostrando ',
    processingText: 'Processing...'
  }

	});

 }