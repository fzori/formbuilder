function drawChart( chartData, oData )
{

	var oDataParsed=[];
		 $.each( oData, function( i, item )
		 {
		 var aux=[];
			if( i > 0 )
			{
				aux=[ item[0] , parseFloat(item[1]) ];
		 	}
		 	else
		 	{
		 		aux=[ item[0] , item[1] ];
		 	}
		 	 	oDataParsed.push( aux );

      	});

	var data = google.visualization.arrayToDataTable( oDataParsed );

        var options = {
          title: chartData.title
        };

		 var chart;

        if( chartData.chartType == "PIECHART" )
        {
         chart = new google.visualization.PieChart(document.getElementById('chart'));
		}
		else
		{
		 	chart = new google.visualization.BarChart(document.getElementById('chart'));

		}

        chart.draw(data, options);

        var  icon  = $( '<a/>' );
        icon.attr('href', '#');
        icon.attr('data-toggle','modal');
        icon.attr('data-target','#addModal');
        var  span = $('<span/>');
        span.addClass('glyphicon glyphicon-signal') ;
        span.attr('aria-hidden', true);
        span.appendTo( icon );
        icon.appendTo($('#report_container'));
}
